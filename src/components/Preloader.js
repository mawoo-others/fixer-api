import React, { Component } from "react";

// Images
import PreloaderImage from "../assets/img/preloader.svg";

class Preloader extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="preloader">
                <img
                    className="preloader__image"
                    src={PreloaderImage}
                    alt="Preloader"
                />
            </div>
        );
    }
}

export default Preloader;
