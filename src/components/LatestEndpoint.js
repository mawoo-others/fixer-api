import React, { Component } from "react";

class LatestEndpoint extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <section className="endpoint__item">
                <h2>Latest</h2>
            </section>
        );
    }
}

export default LatestEndpoint;
