import React, { Component } from "react";

// Promise based HTTP client for the browser and node.js
import axios from "axios";

// Components
import Preloader from "./Preloader";
import Form from "./form/Form";

const data = {
    accessKey: "2bf55b3995cb31786a3b1bbd600f57bf",
    apiBaseUrl: "http://data.fixer.io/api/"
};

class HistoricalEndpoint extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allAvailableCurrencies: {
                isLoaded: false,
                symbolsList: null
            }
        };
    }

    componentDidMount() {
        // Updating all available currencies.
        this.updateAllAvailableCurrencies();
    }

    updateAllAvailableCurrencies() {
        this.sendRequestToAPI(data.apiBaseUrl + "symbols", {
            access_key: data.accessKey
        });
    }

    sendRequestToAPI(url, params) {
        const reference = this;

        axios
            .get(url, {
                params: params
            })
            .then(function(response) {
                const symbolsListObj = response.data.symbols;
                const symbolsListArray = [];

                for (const key in symbolsListObj) {
                    if (symbolsListObj.hasOwnProperty(key)) {
                        const element = symbolsListObj[key];

                        symbolsListArray.push({
                            name: element,
                            symbol: key
                        });
                    }
                }

                reference.setState({
                    allAvailableCurrencies: {
                        isLoaded: true,
                        symbolsList: symbolsListArray
                    }
                });
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    render() {
        const allAvailableCurrenciesIsLoaded = this.state.allAvailableCurrencies
            .isLoaded;
        let preloaderView;
        let formView;

        if (!allAvailableCurrenciesIsLoaded) {
            preloaderView = <Preloader />;
        }

        if (allAvailableCurrenciesIsLoaded) {
            formView = (
                <Form
                    symbolsList={this.state.allAvailableCurrencies.symbolsList}
                />
            );
        }

        return (
            <section className="endpoint__item">
                <h2>Historical</h2>

                {preloaderView}

                {formView}
            </section>
        );
    }
}

export default HistoricalEndpoint;
