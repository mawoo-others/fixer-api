import React, { Component } from "react";

// Styles
import "./SearchRateHistory.css";

class SearchRateHistory extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        let rateHistoryView = (
            <table className="rate-history">
                <thead>
                    <tr>
                        <th>Lp.</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Rate</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.searchRateHistory.map((element, index) => {
                        return (
                            <tr key={index}>
                                <td>{index + 1}</td>
                                <td>{element.date}</td>
                                <td>{element.time}</td>
                                <td>{element.rate}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        );

        return <div>{rateHistoryView}</div>;
    }
}

export default SearchRateHistory;
