import React, { Component } from "react";

// Styles
import "./Form.css";

// Promise based HTTP client for the browser and node.js
import axios from "axios";

// Components
import SearchRateHistory from "../search-rate-history/SearchRateHistory";

const data = {
    accessKey: "2bf55b3995cb31786a3b1bbd600f57bf",
    apiBaseUrl: "http://data.fixer.io/api/",
    currencyBase: "EUR"
};

class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formFieldValue: {
                amount: 1,
                date: "",
                currencyIn: "EUR",
                currencyOut: "PLN"
            },
            currencyOutHistoricalRate: "--"
        };
        this.getInputFieldValue = this.getInputFieldValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.getCurrentDate();

        //
        this.getCurrencyOutHistoricalRateFromSessionStorage();
    }

    getCurrentDate() {
        const date = new Date();
        const newState = Object.assign({}, this.state);

        newState.formFieldValue.date = date
            .toISOString()
            .replace(/T.{1,}/g, "");
        this.setState(newState);
    }

    getInputFieldValue(event, field) {
        const newState = Object.assign({}, this.state);

        newState.formFieldValue[event.target.id] = event.target.value;
        this.setState(newState);
    }

    handleSubmit(event) {
        event.preventDefault();
        //
        this.getCurrencyRateRelativeToCurrencyBase();

        //
        this.getCurrencyOutHistoricalRateFromSessionStorage();
    }

    getCurrencyRateRelativeToCurrencyBase() {
        this.sendRequestToAPI(
            data.apiBaseUrl + this.state.formFieldValue.date,
            {
                access_key: data.accessKey,
                base: data.currencyBase,
                symbols:
                    this.state.formFieldValue.currencyIn +
                    "," +
                    this.state.formFieldValue.currencyOut
            }
        );
    }

    sendRequestToAPI(url, params) {
        const reference = this;

        axios
            .get(url, {
                params: params
            })
            .then(function(response) {
                reference.calcHistoricalRateOfCurrencyOut(response.data.rates);
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    calcHistoricalRateOfCurrencyOut(rates) {
        const newState = Object.assign({}, this.state);
        let currencyInRate = rates[this.state.formFieldValue.currencyIn];
        let currencyOutRate = rates[this.state.formFieldValue.currencyOut];
        let currencyOutHistoricalRate =
            (this.state.formFieldValue.amount * currencyOutRate) /
            currencyInRate;

        newState.currencyOutHistoricalRate =
            this.state.formFieldValue.amount +
            " " +
            this.state.formFieldValue.currencyIn +
            " = " +
            currencyOutHistoricalRate.toFixed(2) +
            " " +
            this.state.formFieldValue.currencyOut;

        this.setState(newState, () => {
            this.saveCurrencyOutHistoricalRateInSessionStorage();
        });
    }

    saveCurrencyOutHistoricalRateInSessionStorage() {
        let date = new Date();

        if (sessionStorage.getItem("searchRateHistory")) {
            const rateHistory = JSON.parse(
                sessionStorage.getItem("searchRateHistory")
            );

            rateHistory.unshift({
                date: date.toLocaleDateString(),
                time: date.toLocaleTimeString(),
                rate: this.state.currencyOutHistoricalRate
            });

            sessionStorage.setItem(
                "searchRateHistory",
                JSON.stringify(rateHistory)
            );
        } else {
            const rateHistory = [];
            rateHistory.push({
                date: date.toLocaleDateString(),
                time: date.toLocaleTimeString(),
                rate: this.state.currencyOutHistoricalRate
            });

            sessionStorage.setItem(
                "searchRateHistory",
                JSON.stringify(rateHistory)
            );
        }
    }

    getCurrencyOutHistoricalRateFromSessionStorage() {
        if (sessionStorage.getItem("searchRateHistory")) {
            const newState = Object.assign({}, this.state);
            const searchRateHistory = JSON.parse(
                sessionStorage.getItem("searchRateHistory")
            );

            newState.searchRateHistory = searchRateHistory;
            this.setState(newState);
        }
    }

    render() {
        const symbolsList = this.props.symbolsList.map((element, index) => {
            return (
                <option key={index} value={element.symbol}>
                    {element.symbol + " - " + element.name}
                </option>
            );
        });

        const historicalRateView = (
            <div className="historical-rate">
                <h3>Historical rate of currency out</h3>
                <p>{this.state.currencyOutHistoricalRate}</p>
            </div>
        );

        let searchRateHistoryView;

        if ("searchRateHistory" in this.state) {
            searchRateHistoryView = (
                <SearchRateHistory
                    searchRateHistory={this.state.searchRateHistory}
                />
            );
        }

        return (
            <div>
                <form id="form" className="form">
                    <div className="form__fields-row">
                        <label className="form__label" htmlFor="amount">
                            Amount
                        </label>
                        <input
                            className="form__field"
                            type="number"
                            name="amount"
                            id="amount"
                            value={this.state.formFieldValue.amount}
                            onChange={this.getInputFieldValue}
                        />
                    </div>
                    <div className="form__fields-row">
                        <label className="form__label" htmlFor="date">
                            Date
                        </label>
                        <input
                            className="form__field"
                            type="date"
                            name="date"
                            id="date"
                            value={this.state.formFieldValue.date}
                            onChange={this.getInputFieldValue}
                        />
                    </div>
                    <div className="form__fields-row">
                        <label className="form__label" htmlFor="currencyIn">
                            Currency In
                        </label>
                        <select
                            className="form__field"
                            name="currencyIn"
                            id="currencyIn"
                            value={this.state.formFieldValue.currencyIn}
                            onChange={this.getInputFieldValue}
                        >
                            {symbolsList}
                        </select>
                    </div>
                    <div className="form__fields-row">
                        <label className="form__label" htmlFor="currencyOut">
                            Currency Out
                        </label>
                        <select
                            className="form__field"
                            name="currencyOut"
                            id="currencyOut"
                            value={this.state.formFieldValue.currencyOut}
                            onChange={this.getInputFieldValue}
                        >
                            {symbolsList}
                        </select>
                    </div>
                    <div className="form__fields-row">
                        <input
                            onClick={this.handleSubmit}
                            type="submit"
                            value="Show rate"
                        />
                    </div>
                </form>

                {historicalRateView}

                {searchRateHistoryView}
            </div>
        );
    }
}

export default Form;
