import React, { Component } from "react";

// Styles
import "./Endpoints.css";

// Components
import LatestEndpoint from "../LatestEndpoint";
import HistoricalEndpoint from "../HistoricalEndpoint";

class Endpoints extends Component {
    constructor(props) {
        super(props);
        this.state = {
            endpoint: [
                {
                    isActive: true,
                    name: "Latest",
                    component: <LatestEndpoint />
                },
                {
                    isActive: false,
                    name: "Historical",
                    component: <HistoricalEndpoint />
                }
            ]
        };
        // This binding is necessary to make `this` work in the callback
        this.getEndpointContent = this.getEndpointContent.bind(this);
    }

    getEndpointContent(event) {
        const endpointID = parseInt(event.target.dataset.endpointId, 10);
        const newState = Object.assign({}, this.state);

        newState.endpoint.forEach((endpoint, index) => {
            if (endpointID === index) {
                endpoint.isActive = true;
            } else {
                endpoint.isActive = false;
            }
        });

        this.setState(newState);
    }

    render() {
        let endpointsListView = (
            <ul onClick={this.getEndpointContent} className="endpoints__list">
                {this.state.endpoint.map((endpoint, index) => {
                    return (
                        <li
                            key={index}
                            className={endpoint.isActive ? "active" : ""}
                            data-endpoint-id={index}
                        >
                            {endpoint.name}
                        </li>
                    );
                })}
            </ul>
        );

        const endpoint = this.state.endpoint;
        let endpointView;

        endpoint.some(function(element) {
            if (element.isActive) {
                endpointView = element.component;
            }

            return element.isActive;
        });

        return (
            <div className="endpoints">
                {endpointsListView}
                {endpointView}
            </div>
        );
    }
}

export default Endpoints;
