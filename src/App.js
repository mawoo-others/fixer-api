import React, { Component } from "react";
import "./App.css";
import Endpoints from "./components/endpoints/Endpoints";

const data = {
    header: {
        name: "Fixer",
        lead:
            "Simple and lightweight API for current and historical foreign exchange rates"
    }
};

class App extends Component {
    render() {
        return (
            <div className="app">
                <header>
                    <h1>{data.header.name}</h1>
                    <p>{data.header.lead}</p>
                </header>
                <main>
                    <Endpoints />
                </main>
            </div>
        );
    }
}

export default App;
